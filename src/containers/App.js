import React, {Component} from 'react';
import Field from '../components/Field/Field'
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Field/>
            </div>
        );
    }
}

export default App;

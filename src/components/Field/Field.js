import React, {Component} from 'react'
import './Field.css'
import Score from '../Score/Score'
import Elements from '../Elements/Elements'

import {
    FIELD_HEIGHT,
    FIELD_WIDTH,
    ELEMENT_SIZE,
    createSnakeElement,
    DIRECTIONS,
    changeDirection, newPositionBasedOnBorders, changeHeadPosition, isSnakeBiteItself, moveSnake, addRandomSnakeFood
} from '../../utils/utils'

class Field extends Component {
    state = {
        snakeElements: [
            createSnakeElement(10, 10, true)
        ],
        snakeFoods: [],
        tick: 0,
        direction: DIRECTIONS.DIR_RIGHT
    }

    ifDirectionChanged = (key) => {
        let newDirection = changeDirection(key, this.state.direction)

        if (newDirection !== undefined && newDirection !== this.state.direction) {
            this.setState({direction: newDirection})
        }
    }

    ifBorderReached = () => {
        const snakeHead = this.state.snakeElements[0]
        const [posX, posY] = newPositionBasedOnBorders(snakeHead.posX, snakeHead.posY)

        if (posX !== snakeHead.posX || posY !== snakeHead.posY) {
            const newSnakeElements = changeHeadPosition(this.state.snakeElements, posX, posY)

            this.setState({snakeElements: newSnakeElements})
        }
    }

    onKeyDownHandler = (event) => {
        event.preventDefault()

        this.ifDirectionChanged(event.key)
    }

    timeTickHandler = () => {
        let tick = this.state.tick + 1
        this.ifBorderReached();
        let biteItself = isSnakeBiteItself(this.state.snakeElements)

        if (biteItself) {
            console.log("Bite itself!!!")
        }

        let [newSnakeElements, newSnakeFoods] = moveSnake(this.state.snakeElements, this.state.snakeFoods, this.state.direction)

        if (tick === 30) {
            newSnakeFoods = addRandomSnakeFood(newSnakeFoods)
            tick = 0
        }

        if (newSnakeFoods !== undefined) {
            this.setState({snakeElements: newSnakeElements, snakeFoods: newSnakeFoods, tick: tick})
        } else {
            this.setState({snakeElements: newSnakeElements, tick: tick})

        }
    }

    componentDidMount() {
        document.addEventListener('keydown', this.onKeyDownHandler);
        this.interval = setInterval(() => this.timeTickHandler(), 100);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.onKeyDownHandler);
        clearInterval(this.interval);
    }

    render() {
        return <svg width={FIELD_WIDTH * ELEMENT_SIZE} height={FIELD_HEIGHT * ELEMENT_SIZE} className="Field">
            <Elements type="snakeParts" snakeFoods={this.state.snakeElements}/>
            <Elements type="snakeFoods" snakeFoods={this.state.snakeFoods}/>
            <Score score={this.state.snakeElements.length - 1}/>
        </svg>
    }
}

export default Field
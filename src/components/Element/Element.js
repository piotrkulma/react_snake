import React from 'react'
import './Element.css'

const elementColor = (type) => {
    switch (type) {
        case 0:
            return "red"
        case 1:
            return "blue"
        default:
            return "green"
    }
}

const elementStyle = (type) => {
    const style = {"fill": elementColor(type)}

    return style
}

const element = props => (
    <circle
        style={elementStyle(props.type)}
        cx={props.posX}
        cy={props.posY}
        r="5"/>
)

export default element
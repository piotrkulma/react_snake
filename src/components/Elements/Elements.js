import React, {Fragment} from 'react'
import Element from '../Element/Element'
import {ELEMENT_SIZE} from "../../utils/utils";

const getElementType = (element, typeName) => {
    let type;

    if (typeName === 'snakeParts') {
        type = element.isHead ? 0 : 1
    } else {
        type = "2"
    }

    return type
}

const elements = (props) => (
    <Fragment>
        {
            props.snakeFoods.map((element, index) => {
                return (
                    <Element
                        key={props.type + index}
                        posX={element.posX * (ELEMENT_SIZE)}
                        posY={element.posY * (ELEMENT_SIZE)}
                        type={getElementType(element, props.type)}/>
                )
            })
        }
    </Fragment>
)

export default elements
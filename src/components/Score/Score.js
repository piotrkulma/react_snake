import React from 'react'
import './Score.css'

const score = (props) => (
    <text className="Score" x="5" y="15">SCORE {props.score}</text>
)

export default score
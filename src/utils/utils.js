const FIELD_WIDTH = 40
const FIELD_HEIGHT = 30
const ELEMENT_SIZE = 10

const DIRECTIONS = {
    DIR_UP: 0,
    DIR_RIGHT: 1,
    DIR_DOWN: 2,
    DIR_LEFT: 3
}

const KEYS = {
    KEY_UP: 'ArrowUp',
    KEY_DOWN: 'ArrowDown',
    KEY_LEFT: 'ArrowLeft',
    KEY_RIGHT: 'ArrowRight',
    KEY_SPACE: ' '
}

const createSnakeElement = (posX, posY, isHead = false) => {
    return {posX: posX, posY: posY, isHead: isHead}
}

const createSnakeFood = (posX, posY) => {
    return {posX: posX, posY: posY}
}

const newPositionBasedOnDirection = (posX, posY, direction) => {
    let [newPosX, newPosY] = [posX, posY]

    switch (direction) {
        case DIRECTIONS.DIR_UP:
            newPosY -= 1
            break
        case DIRECTIONS.DIR_RIGHT:
            newPosX += 1
            break
        case DIRECTIONS.DIR_DOWN:
            newPosY += 1
            break
        case DIRECTIONS.DIR_LEFT:
            newPosX -= 1
            break
        default:
            throw new Error('Direction [' + direction + '] should be from 0 to 3 range')
    }

    return [newPosX, newPosY]
}

const moveHead = (head, direction) => {
    const [posX, posY] = newPositionBasedOnDirection(head.posX, head.posY, direction)
    return createSnakeElement(posX, posY, head.isHead)
}

const isIntersects = (x1, y1, x2, y2) => {
    return x1 === x2 && y1 === y2
}

const isSnakeBiteItself = (snakeElement) => {
    if(snakeElement.length === 1) {
        return false
    }
    const head = snakeElement[0]

    for(let i=1; i<snakeElement.length; i++) {
        if(isIntersects(snakeElement[i].posX, snakeElement[i].posY, head.posX, head.posY)) {
            return true
        }
    }

    return false;
}

const isSnakeIntersectsFood = (snakeElement, snakeFood) => {
    return isIntersects(snakeElement.posX, snakeElement.posY, snakeFood.posX, snakeFood.posY)
}

const isFeed = (snakeElements, snakeFood) => {
    for (let snakeElementIndex = 0; snakeElementIndex < snakeElements.length; snakeElementIndex++) {
        for (let snakeFoodIndex = 0; snakeFoodIndex < snakeFood.length; snakeFoodIndex++) {
            if (isSnakeIntersectsFood(snakeElements[snakeElementIndex], snakeFood[snakeFoodIndex])) {
                return snakeFoodIndex
            }
        }
    }

    return -1
}

const changeHeadPosition = (snakeElements, posX, posY) => {
    let newSnakeElements = [];

    for (let i = 0; i < snakeElements.length; i++) {
        if (snakeElements[i].isHead) {
            newSnakeElements.push(createSnakeElement(posX, posY, true))
        } else {
            newSnakeElements.push(
                createSnakeElement(
                    snakeElements[i].posX,
                    snakeElements[i].posY, false))
        }
    }

    return newSnakeElements
}

const removeSnakeFoodElement = (snakeFoods, snakeFoodIndexToRemove) => {
    let newSnakeFoods = [];

    for (let i = 0; i < snakeFoods.length; i++) {
        if (i !== snakeFoodIndexToRemove) {
            newSnakeFoods.push(snakeFoods[i])
        }
    }

    return newSnakeFoods
}

const moveSnake = (snakeElements, snakeFoods, direction) => {
    let newSnakeFoods
    let newSnakeElements = []

    for (let i = 0; i < snakeElements.length; i++) {
        if (snakeElements[i].isHead) {
            newSnakeElements.push(moveHead(snakeElements[i], direction))
        } else {
            newSnakeElements.push(
                createSnakeElement(
                    snakeElements[i - 1].posX,
                    snakeElements[i - 1].posY, false))
        }
    }

    let snakeFoodIndexToRemove = isFeed(snakeElements, snakeFoods);

    if (snakeFoodIndexToRemove !== -1) {
        newSnakeElements.push(createSnakeElement(
            snakeElements[snakeElements.length - 1].posX,
            snakeElements[snakeElements.length - 1].posY, false))

        newSnakeFoods = removeSnakeFoodElement(snakeFoods, snakeFoodIndexToRemove)
    } else {
        newSnakeFoods = [...snakeFoods]
    }

    return [newSnakeElements, newSnakeFoods]
}

const changeDirection = (key, currentDirection) => {
    let newDirection;
    switch (key) {
        case KEYS.KEY_UP:
            newDirection = DIRECTIONS.DIR_UP
            break
        case KEYS.KEY_RIGHT:
            newDirection = DIRECTIONS.DIR_RIGHT
            break
        case KEYS.KEY_DOWN:
            newDirection = DIRECTIONS.DIR_DOWN
            break
        case KEYS.KEY_LEFT:
            newDirection = DIRECTIONS.DIR_LEFT
            break
        default:
            break
    }

    if (newDirection !== undefined) {
        if(Math.abs(newDirection - currentDirection) === 2) {
            newDirection = undefined
        }
    }

    return newDirection;
}

const newPositionBasedOnBorders = (posX, posY) => {
    let [newPosX, newPosY] = [posX, posY]

    if (posX >= FIELD_WIDTH) {
        newPosX = 0
    } else if (posX < 0) {
        newPosX = FIELD_WIDTH
    }

    if (posY >= FIELD_HEIGHT) {
        newPosY = 0
    } else if (posY < 0) {
        newPosY = FIELD_HEIGHT
    }

    return [newPosX, newPosY]
}

const randomInt = (min, max) => {
    return min + Math.floor((max - min) * Math.random());
}

const addRandomSnakeFood = (snakeFoods) => {
    const newSnakeFoods = [...snakeFoods, createSnakeFood(randomInt(2, FIELD_WIDTH - 2), randomInt(2, FIELD_HEIGHT - 2))]

    return newSnakeFoods
}

export {
    FIELD_WIDTH,
    FIELD_HEIGHT,
    ELEMENT_SIZE,
    DIRECTIONS,
    isSnakeBiteItself,
    createSnakeElement,
    createSnakeFood,
    changeHeadPosition,
    moveSnake,
    changeDirection,
    newPositionBasedOnBorders,
    addRandomSnakeFood
}